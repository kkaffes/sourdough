#include <iostream>

#include "controller.hh"
#include "timestamp.hh"

using namespace std;

/* Default constructor */
Controller::Controller( const bool debug )
  : debug_( debug ), cwnd(10), cwnd_cnt(0), var_window() 
{
  var_window.num_samples = 0;
}

/* Get current window size, in datagrams */
unsigned int Controller::window_size( void )
{
  /* Default: fixed window size of 100 outstanding datagrams */
  unsigned int the_window_size = cwnd;

  if ( debug_ ) {
    cerr << "At time " << timestamp_ms()
	 << " window size is " << the_window_size << endl;
  }

  return the_window_size;
}

/* A datagram was sent */
void Controller::datagram_was_sent( const uint64_t sequence_number,
				    /* of the sent datagram */
				    const uint64_t send_timestamp )
                                    /* in milliseconds */
{
  /* Default: take no action */

  if ( debug_ ) {
    cerr << "At time " << send_timestamp
	 << " sent datagram " << sequence_number << endl;
  }
}


void shift_window(window* var_window, uint64_t rtt_samp) {

  unsigned int num_samps = var_window->num_samples;

  // shift all samples down the window to make room for the new sample
  for (unsigned int i = 0; i < num_samps-1; i++) {
    var_window->rtt_samples[i] = var_window->rtt_samples[i+1];
  }

  // insert the new sample at the end
  var_window->rtt_samples[num_samps-1] = rtt_samp;

}


double compute_variance(window* var_window) {
  double mean = 0;
  for (unsigned int i = 0; i < var_window->num_samples; i++) {
    mean += var_window->rtt_samples[i];
  }
  mean /= var_window->num_samples;

  double var = 0;
  for(unsigned int i = 0; i < var_window->num_samples; i++ ) {
    var += (var_window->rtt_samples[i] - mean) * (var_window->rtt_samples[i] - mean);
  }
  var /= var_window->num_samples;
  return var;
}

/* An ack was received */
void Controller::ack_received( const uint64_t sequence_number_acked,
			       /* what sequence number was acknowledged */
			       const uint64_t send_timestamp_acked,
			       /* when the acknowledged datagram was sent (sender's clock) */
			       const uint64_t recv_timestamp_acked,
			       /* when the acknowledged datagram was received (receiver's clock)*/
			       const uint64_t timestamp_ack_received )
                               /* when the ack was received (by sender) */
{

  /* get the RTT sample */
  uint64_t rtt_samp = timestamp_ack_received - send_timestamp_acked;

  // insert the new sample into the window
  if (var_window.num_samples < WINDOW_SIZE) {
    var_window.rtt_samples[var_window.num_samples] = rtt_samp;
    var_window.num_samples++;
  } else {
    shift_window(&var_window, rtt_samp);
  }

  // compute the variance of the samples in the window
  double var = compute_variance(&var_window);

  /* Decrease cwnd if variance of the window is too large
   * and rtt is increasing over time. */
  if ((var > VAR_THRESH) && (var_window.rtt_samples[WINDOW_SIZE - 1] > var_window.rtt_samples[WINDOW_SIZE - 2]))  {
    cwnd_cnt = 0;
    if (cwnd > 1) {
        cwnd /= CWND_DEC;
    }
    cwnd = (cwnd < 1) ? 1 : cwnd; // min value of cwnd = 1 
  } else {
    /* Otherwise increase cwnd */
    cwnd_cnt += 1;
    if (cwnd_cnt >= cwnd*CWND_CNT_GAIN) {
      cwnd += CWND_INC;
      cwnd_cnt = 0;
    }
  }

  if ( debug_ ) {
    cerr << "At time " << timestamp_ack_received
	 << " received ack for datagram " << sequence_number_acked
	 << " (send @ time " << send_timestamp_acked
	 << ", received @ time " << recv_timestamp_acked << " by receiver's clock)"
	 << endl;
  }
}

/* How long to wait (in milliseconds) if there are no acks
   before sending one more datagram */
unsigned int Controller::timeout_ms( void )
{
  return 100; /* 100 ms timeout */
}
