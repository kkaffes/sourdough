#ifndef CONTROLLER_HH
#define CONTROLLER_HH

#include <cstdint>

#define CWND_INC 2
#define CWND_DEC 2
#define CWND_CNT_GAIN 0.2

#define WINDOW_SIZE 7
#define VAR_THRESH 50

struct window {
  uint64_t rtt_samples[WINDOW_SIZE];
  unsigned int num_samples;
};


/* Congestion controller interface */

class Controller
{
private:
  bool debug_; /* Enables debugging output */

  /* Add member variables here */
  unsigned int cwnd; 
  unsigned int cwnd_cnt; // incremented by 1 for each ACK, used to pace the additive increase of cwnd 
  window var_window;

public:
  /* Public interface for the congestion controller */
  /* You can change these if you prefer, but will need to change
     the call site as well (in sender.cc) */

  /* Default constructor */
  Controller( const bool debug );

  /* Get current window size, in datagrams */
  unsigned int window_size( void );

  /* A datagram was sent */
  void datagram_was_sent( const uint64_t sequence_number,
			  const uint64_t send_timestamp );

  /* An ack was received */
  void ack_received( const uint64_t sequence_number_acked,
		     const uint64_t send_timestamp_acked,
		     const uint64_t recv_timestamp_acked,
		     const uint64_t timestamp_ack_received );

  /* How long to wait (in milliseconds) if there are no acks
     before sending one more datagram */
  unsigned int timeout_ms( void );

  /* Action to take on timeout */
  void timeout( void );
};

#endif
